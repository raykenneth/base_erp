<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

//super admin
Route::get('/admin/customer-management','CustomerManagementController@index');
Route::get('/admin/customer-management/getCompanies','CustomerManagementController@getCompanies');
Route::post('/admin/customer-management/checkCustomerUsername','CustomerManagementController@checkCustomerUsername');
Route::post('/admin/customer-management/saveCustomer','CustomerManagementController@saveCustomer');
Route::post('/admin/customer-management/viewCustomerDetails','CustomerManagementController@viewCustomerDetails');
Route::post('/admin/customer-management/updateCustomerProfile','CustomerManagementController@updateCustomerProfile');
Route::get('/admin/release-notes','ReleaseNotesController@index');
Route::get('/admin/release-notes/getReleaseNotesList','ReleaseNotesController@getReleaseNotesList');
Route::post('/admin/release-notes/addReleaseNotes','ReleaseNotesController@addReleaseNotes');
Route::post('/admin/release-notes/viewReleaseNotes','ReleaseNotesController@viewReleaseNotes');
Route::post('/admin/release-notes/deleteReleaseNotes','ReleaseNotesController@deleteReleaseNotes');

// Company Management
Route::group(['middleware' => ['companymanagement', 'auth']], function () 
{
	//User 
	Route::get('/users', 'UserController@index');
	Route::get('/users/getAllUsers', 'UserController@getAllUsers');	
	Route::post('/users/checkUsername', 'UserController@checkUsername');
	Route::get('/users/getPosition', 'PositionController@getAllPosition');
	Route::get('/users/getDepartment', 'DepartmentController@getAllDepartment');
	Route::get('/users/getUserRoles', 'RoleController@getUserRoles');
	Route::post('/users/saveUserInformation', 'UserController@saveUserInformation');
	Route::post('/users/getUserDetails', 'UserController@getUserDetails');
	Route::post('/users/updateUserDetails', 'UserController@updateUserDetails');

	//Position
	Route::get('/positions','PositionController@index');
	Route::get('/positions/getPosition','PositionController@getPosition');
	Route::post('/positions/addPosition','PositionController@addPosition');
	Route::post('/positions/getPositionDetails', 'PositionController@getPositionDetails');
	Route::post('/positions/updatePosition', 'PositionController@updatePosition');

	// Department
	Route::get('/departments','DepartmentController@index');
	Route::get('/departments/getDepartment','DepartmentController@getDepartment');
	Route::post('/departments/addDepartment','DepartmentController@addDepartment');
	Route::post('/departments/getDepartmentDetails', 'DepartmentController@getDepartmentDetails');
	Route::post('/departments/updateDepartment', 'DepartmentController@updateDepartment');
	//Role 
	Route::get('/roles', 'RoleController@index');
	Route::get('/roles/getAllRoles', 'RoleController@getAllRoles');
	Route::post('/roles/getRoleDetails', 'RoleController@getRoleDetails');
	Route::post('/roles/updateRolePermissions', 'RoleController@updateRolePermissions');
	Route::post('/roles/saveRoleInformation', 'RoleController@saveRoleInformation');
	
	//Company
	Route::get('/company-profile', 'CustomerManagementController@companyProfile');
	Route::post('/company-profile/updateCompanyProfile', 'CustomerManagementController@updateCompanyProfile');
	Route::post('/company-profile/uploadCompanyLogo','CustomerManagementController@updateCompanyLogo');
	Route::get('/company-profile/deleteCompanyLogo/{id}','CustomerManagementController@deleteCompanyLogo');
	Route::post('/company-profile/updateUISettings','CustomerManagementController@updateUISettings');
	Route::post('/company-profile/closingOfBooks','CustomerManagementController@closingOfBooks');
	// UI Settings
	Route::get('/ui-settings', 'CustomerManagementController@uiSettings');
	//Approval
	Route::get('/approval', 'ApprovalController@index');
	Route::get('/approval/getApprovals', 'ApprovalController@getApprovals');
	Route::get('/approval/getFunctions', 'ApprovalController@getFunctions');
	Route::get('/approval/getDepartments', 'ApprovalController@getDepartments');
	Route::post('/approval/checkApproval', 'ApprovalController@checkApproval');
	Route::post('/approval/addUserApproval', 'ApprovalController@addUserApproval');
	Route::post('/approval/getUserChecker', 'ApprovalController@getUserChecker');
	Route::post('/approval/viewUserChecker', 'ApprovalController@viewUserChecker');
	Route::post('/approval/saveUserCheckerApproval', 'ApprovalController@saveUserCheckerApproval');
	Route::post('/approval/getUserApprover', 'ApprovalController@getUserApprover');
	Route::post('/approval/viewUserApprover', 'ApprovalController@viewUserApprover');
	Route::post('/approval/saveUserApproverApproval', 'ApprovalController@saveUserApproverApproval');
	Route::post('/approval/getViewUserApproval', 'ApprovalController@getViewUserApproval');
	Route::post('/approval/getViewApproval', 'ApprovalController@getViewApproval');
	
});