<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_ids 	=	"1,2";
    	$check  	=	DB::table('user_roles')
				        	->where('id', 1)
					        ->count();
		if ($check >= 1) {
	        DB::table('user_roles')
	        	->where('id', 1)
		        ->update([
		        	'permission' => $role_ids
		        ]);
		}else{
			DB::table('user_roles')
		        ->insert([
		        	'id' 		 => 0,
		        	'name' 		 => 'Unassigned',
		        	'permission' => 0,
		        	'company_id' => 0,
		        	'status' 	 => 99,
		        	'created_at' => date('Y-m-d'),
		        	'created_by' => 1
		        ]);
			DB::table('user_roles')
		        ->insert([
		        	'id' 		 => 1,
		        	'name' 		 => 'Super User',
		        	'permission' => $role_ids,
		        	'company_id' => 0,
		        	'status' 	 => 99,
		        	'created_at' => date('Y-m-d'),
		        	'created_by' => 1
		        ]);
		}
    }
}
