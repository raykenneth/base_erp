<?php

use Illuminate\Database\Seeder;

class UserRolesPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$permission_names 	=	[
			    					'No Permission', 
			    					'Company Management'
			    				];
		$array	=	[];
		foreach ($permission_names as $key => $value) {
			$array[]  	=	[	
								'id'  			  => $key,
								'permission_name' => $value
							];
		}
		DB::table('user_roles_permission')->delete();
        DB::table('user_roles_permission')
        ->insert($array);
    }
}
