<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalTableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('function_id');
            $table->integer('department_id');
            $table->integer('company_id');
            $table->dateTime('created_at');
            $table->dateTime('updated_at')->nullable();
            $table->integer('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approval_table');
    }
}
