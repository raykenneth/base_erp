<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name',255);
            $table->string('company_email',255);
            $table->string('contact_person',255)->nullable();
            $table->string('contact_number',50);
            $table->string('tin_number',20);
            $table->string('fax_number',100)->nullable();
            $table->string('address_number',100)->nullable();
            $table->string('street',100)->nullable();
            $table->string('barangay',100)->nullable();
            $table->string('city',100)->nullable();
            $table->string('region',100)->nullable();
            $table->string('country',100)->nullable();
            $table->integer('zip_code')->nullable();
            $table->string('billing_street',100)->nullable();
            $table->string('billing_city',100)->nullable();
            $table->string('billing_region',100)->nullable();
            $table->string('billing_country',100)->nullable();
            $table->integer('billing_zip_code')->nullable();
            $table->integer('user_count');
            $table->string('subscription_type',100)->nullable();
            $table->text('company_image')->nullable();
            $table->text('remarks')->nullable();
            $table->enum('status',['0',['1']])->comment('0 = Active 1 = Inactive')->default('0');
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
