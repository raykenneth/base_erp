@extends('layouts.auth')

@section('content')
<div class="container-login100" style="background-image: url('../images/bg-01.jpg');">
    <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
        <form class="login100-form validate-form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
           
            <span class="login100-form-title p-b-49">
                <div><img alt="Logo" height="100" src="{{ asset('images/logo/logo.jpg') }}"/></div>
            </span>

            @if(session('status'))
                <div class="alert alert-success" align="center">
                    <font color="red"><b><i class="fa fa-exclamation-circle" aria-hidden="true"></i> {{ session('status') }}</b></font>
                </div>
            @endif

            <br/>
      
            <div class="wrap-input100 validate-input m-b-23" data-validate = "Email is required">
                <span class="label-input100">Email</span>
                <input class="input100" type="email" name="email" placeholder="Type your Email">
                <span class="focus-input100" data-symbol="&#xf206;"></span>
            </div>

            <div class="container-login100-form-btn">
                <div class="wrap-login100-form-btn">
                    <div class="login100-form-bgbtn"></div>
                    <button type="submit" class="login100-form-btn">
                        Send Password Reset Link
                    </button>
                </div>
            </div>
            <br/>
            <div class="flex-c-m">
                <a href="http://www.atgscorp.com" target="_blank"> 
                    Powered by: ATGS Corp
            </div>

<!--             <div class="txt1 text-center p-t-54 p-b-20">
                <span>
                    Or Sign Up Using
                </span>
            </div>

            <div class="flex-c-m">
                <a href="#" class="login100-social-item bg1">
                    <i class="fa fa-facebook"></i>
                </a>

                <a href="#" class="login100-social-item bg2">
                    <i class="fa fa-twitter"></i>
                </a>

                <a href="#" class="login100-social-item bg3">
                    <i class="fa fa-google"></i>
                </a>
            </div> -->
              


        </form>
    </div>
</div>
@endsection
