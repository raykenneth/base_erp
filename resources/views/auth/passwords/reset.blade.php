@extends('layouts.auth')

@section('content')
<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
    <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
        <form class="login100-form validate-form" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            @if(session('status'))
                <div class="alert alert-success">
                    <p><font color="red">{{ session('status') }} </p>
                </div>
            @endif
            <input type="hidden" name="token" value="{{ $token }}">

            <span class="login100-form-title p-b-49">
                <div><img alt="Logo" height="100" src="{{ asset('images/logo/logo.jpg') }}"/></div>
            </span>
      
            <div class="wrap-input100 validate-input m-b-23" data-validate = "Username is required">
                <span class="label-input100">Username</span>
                <input class="input100" type="name" name="name" placeholder="Type your Username">
                <span class="focus-input100" data-symbol="&#xf206;"></span>
            </div>

            <div class="wrap-input100 validate-input" data-validate="Password is required">
                <span class="label-input100">Password</span>
                <input class="input100" type="password" name="password" placeholder="Type your password">
                <span class="focus-input100" data-symbol="&#xf190;"></span>
            </div>

            <br/>

            <div class="wrap-input100 validate-input" data-validate="Password is required">
                <span class="label-input100">Confirm Password</span>
                <input class="input100" id="password-confirm" type="password" name="password_confirmation" placeholder="Type your password" required>
                <span class="focus-input100" data-symbol="&#xf190;"></span>
            </div>
            
            <br>
            <div class="container-login100-form-btn">
                <div class="wrap-login100-form-btn">
                    <div class="login100-form-bgbtn"></div>
                    <button type="submit" class="login100-form-btn">
                        Reset Password
                    </button>
                </div>
            </div>
            <br/>
            <div class="flex-c-m">
                <a href="http://www.atgscorp.com" target="_blank"> 
                    Powered by: ATGS Corp
            </div>

<!--             <div class="txt1 text-center p-t-54 p-b-20">
                <span>
                    Or Sign Up Using
                </span>
            </div>

            <div class="flex-c-m">
                <a href="#" class="login100-social-item bg1">
                    <i class="fa fa-facebook"></i>
                </a>

                <a href="#" class="login100-social-item bg2">
                    <i class="fa fa-twitter"></i>
                </a>

                <a href="#" class="login100-social-item bg3">
                    <i class="fa fa-google"></i>
                </a>
            </div> -->
              


        </form>
    </div>
</div>
@endsection
