<form id="edit-department-form" method="post" data-parsley-validate>
<div class="row"> 
  <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="id" value="{{$department->id}}" readonly>
      <div class="form-group">
          <span id="department_name_label"></span>
          <label class="department_name_label">Name</label>
          <span id="department_name_view"></span>
          <p class="department_name_view">{{ $department->name }}</p>
      </div>
  </div>
  <button type="button" class="bt`n btn-primary" id="edit-department" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>  
  <button type="submit" class="btn btn-primary" id="update-department" style="display: none" title="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>

</div>
</form>
<script type="text/javascript">
  $(document).ready(function(){
    
        $('div').on('click', 'button#edit-department', function(e){
            $("form#edit-department-form").parsley();
            $('.modal-title').text('Edit Department');
            var department_name_view                   = $('.department_name_view').text();
            $('#department_name_label').append('<label>Name<span style="color:red">*</span></label>');

            $('#department_name_view').append('<input type="text" class="form-control" name="name" id="chart_of_account_name" value="'+department_name_view+'" required/>');
            $('.department_name_label').hide();
            $('.department_name_view').hide();
            $(this).hide();
            $('#update-department').show();
            e.preventDefault();
            return false;
        })

        
       
         // Update Chart of Account form
        $('div').on('submit','form#edit-department-form',function(e){
         
          var formData = $(this).serialize();
          swal({
            title: "Are you sure?",
            text: "Once saved, you will not be able to change your key information!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willSave) => {
            if (willSave) {
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
              })
              $.ajax({
                 url: '/departments/updateDepartment',
                 type: "POST",
                 data: formData,
                 // If success
                 success: function(response) {
                     if (response != '') {
                       $('#get-department-table').DataTable().destroy();
                        getDepartmentTable();
                        $('#edit-department-form').trigger("reset");
                        swal("Department has been updated!", {
                          icon: "success",
                        });
                        $('#view-department-details').modal('hide');
                     }
                 },
                
              });
            } else {
              swal("Save Aborted");
            }
          });
          e.preventDefault();
          return false;
       })
  });
</script>
