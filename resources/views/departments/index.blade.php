
@extends('layouts.app')
@section('content') 
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">{{ strtoupper(str_replace("-", " ", Request::segment(1))) }}</h1>
                    {{ breadCrumbs() }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header card-header-global">
                        <h3><i class="fa fa-sitemap"></i> Departments List  <span title=" This module list the departments of your company  "><i class=" fa fa-info-circle" style="color: #dddddd"></i></span> <span class="pull-right"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add-department-modal"><i class="fa fa-plus" aria-hidden="true"></i></button></span></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive get-department-table">
                            <table id="get-department-table" class="table table-bordered table-sm">
                                <thead class="thead-global">
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end card-->          
            </div>
        </div>
    </div>
    <!-- END container-fluid -->
</div>
<!-- END content -->
@include('departments.modal.add-department')
@include('departments.modal.view-department-modal')
<script type="text/javascript">
    //function in calling datatable ajax
    function getDepartmentTable(){
      var table = $('#get-department-table').DataTable({
               ajax: {
                   url: '/departments/getDepartment',
                   dataSrc: ''
               },
               columns: [ 
                    { data: '#' },
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'action' }
               ]
           });
    }
    
    $(document).ready(function(){
        //datatable ajax
        setTimeout(
          function() 
          {
            getDepartmentTable();
            //search each column datatable
            $('#get-department-table thead th').each(function(e) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
            });
            $('#get-department-table').DataTable().columns().every(function() {
                var that = this;
                $( 'input', this.header() ).on('keyup change', function () {
                    if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                    }
                });
            });
          }, 1000);
        
        //view vat type
        $('div').on('click', '.view-department-details', function(e){
            $('#view-department-details').modal('show');
            $('.department-details').html('');
            $('.modal-title').html('');
            var id = this.id;
            $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            })
            $.ajax({
               url: '/departments/getDepartmentDetails',
               type: "POST",
               dataType: "HTML",
               data: {id:id},
               beforeSend: function() {
                    $('.loader').show();
               },
               success: function(response) {
                   if (response != '') {
                      $('.modal-title').html('View Department');
                      $('.department-details').html(response);
                   }
               },
               complete: function() {
                    $('.loader').hide();
               }
            });
            e.preventDefault();
            return false;
        });    
    })
    
</script>
@endsection