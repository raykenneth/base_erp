<div class="modal fade" id="copy-user-role" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
            
                <h4 class="modal-title" id="myModalLabel"> Copy User Role
                </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table id="copy-user-role-table" class="table table-striped table-condensed" style="width: 100%;">
                    <thead class="thead-global">
                        <tr>
                            <td>User</td>
                            <td>Role</td>
                            <td>Option</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var table = $('#copy-user-role-table').DataTable({
            ajax: {
                url: '/users/getUserRoles',
                dataSrc: ''
            },
            columns: [ 
                { data: 'user' },
                { data: 'role' },
                { data: 'option' }
            ]
        });

        $('div').on('click', '.choose-role', function(e){
            var role_id = this.id;
            var role_name = $(this).data("id");
            swal({
                 text: "Are you sure you want to pick the role of this user?",
                 icon: "warning",
                 buttons: true,
                 dangerMode: true,
               })
           .then((willSave) => {
             if (willSave) {
                $('#role_id').val(role_id);
                $('#role_name').val(role_name);
                swal("User Role selected!", {
                    icon: "success",
                });
             } else {
                swal("Canceled!");
             }
            });  
            e.preventDefault();
            return false;
        })
    })
</script>