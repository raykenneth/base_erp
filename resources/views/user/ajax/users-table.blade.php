
<table id="users-table" class="table table-striped table-bordered table-sm">
    <thead class="thead-global">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Department</th>
            <th>Position</th>
            <th>Role</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
	//function in calling datatable ajax
    function getUsersTable(){
      var table = $('#users-table').DataTable({
               ajax: {
                   url: '/users/getAllUsers',
                   dataSrc: ''
               },
               columns: [ 
                    { data: '#' },
                    { data: 'name' },
                    { data: 'username' },
                    { data: 'email' },
                    { data: 'department' },
                    { data: 'position' },
                    { data: 'role' },
                    { data: 'status' },
                    { data: 'actions' }
               ]
           });
    }
    
    $(document).ready(function(){
        //datatable ajax
        setTimeout(
          function() 
          {
            getUsersTable();
            //search each column datatable
    	    $('#users-table thead th').each(function(e) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
            });
            $('#users-table').DataTable().columns().every(function() {
                var that = this;
                $( 'input', this.header() ).on('keyup change', function () {
                    if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                    }
                });
            });
          }, 1500);
    })
    
</script>