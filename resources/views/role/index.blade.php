@extends('layouts.app')
@section('content') 
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">ACCESS ROLES</h1>
                    {{ breadCrumbs() }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header card-header-global">
                        <h3><i class="fa fa-key"></i> Access Roles List<span class="pull-right"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add_role_modal"><i class="fa fa-plus" aria-hidden="true"></i></button></span></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive get-roles-table">
                            <table id="roles-table" class="table table-striped table-bordered table-sm">
                                <thead class="thead-global">
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end card-->          
            </div>
        </div>
    </div>
    <!-- END container-fluid -->
</div>
<!-- END content -->
@include('role.modal.view_role_details_modal');
@include('role.modal.add_role_modal');
<script type="text/javascript">
    //function in calling datatable ajax
    function getRolesTable(){
      var table = $('#roles-table').DataTable({
               ajax: {
                   url: '/roles/getAllRoles',
                   dataSrc: ''
               },
               columns: [ 
                    { data: '#' },
                    { data: 'name' },
                    { data: 'created_at' },
                    { data: 'updated_at' },
                    { data: 'status' },
                    { data: 'action' }
               ]
           });
    }
    
    $(document).ready(function(){
        //datatable ajax
        setTimeout(
        function() 
        {
            getRolesTable();
            //search each column datatable
            $('#roles-table thead th').each(function(e) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
            });
            $('#roles-table').DataTable().columns().every(function() {
                var that = this;
                $( 'input', this.header() ).on('keyup change', function () {
                    if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                    }
                });
            });
        }, 1000);

        //view companies modal
        $('div').on('click', '.view-role-details', function(e){
            $('#view-role-details').modal('show');
            $('.role-details').html('');
            var id = this.id;
            $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            })
            $.ajax({
               url: '/roles/getRoleDetails',
               type: "POST",
               dataType: "HTML",
               data: {id:id},
               beforeSend: function() {
                    $('.loader').show();
               },
               success: function(response) {
                   if (response != '') {
                      $('.role-details').html(response);
                   }
               },
               complete: function() {
                    $('.loader').hide();
               }
            });
            e.preventDefault();
            return false;
        });
    })
    
</script>
@endsection