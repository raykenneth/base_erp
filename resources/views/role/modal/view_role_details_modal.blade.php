<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="view-role-details" aria-hidden="true" id="view-role-details">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">View Access Role</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>              
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="loader" style="display: none; text-align: center; margin: 0;">
                             <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            <span class="sr-only">Loading...</span>
                        </div>
                        <div class="role-details">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Include SmartWizard JavaScript source -->
<script src="{{ asset('/js/sweetalert.js') }}"></script>