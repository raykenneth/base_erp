<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-header">
                <h3><i class="fa fa-key"></i> Access Roles</h3>
            </div>
            <div class="card-body">
                <form id="edit-roles-form">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <div class="row">
                                <div class="col-lg-6">
                                @if($role != 99)
                                    <input type="text" name="name" value="{{ $role_name }}" class="form-control" disabled="disabled" id="role-name">
                                @else
                                    <input type="text" name="name" value="{{ $role_name }}" class="form-control" readonly="readonly">
                                @endif
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-9">
                                    <input type="hidden" name="role_id" value="{{ $role_id }}" readonly="readonly">
                                    <?php $counter = 0; ?>
                                    @foreach($permission as $key => $value)
                                        <?php $counter++; ?> 
                                        <!-- For the meantime, hide Inventory Issuance -->
                                        @if($value->id == 41)
                                        @else
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input permissions" type="checkbox" <?php echo $checked = ( in_array( $counter-1, $role_details ) ? "checked" : '' ); ?> name="permissions[]" value="{{ $value->id }}" disabled> {{ $value->permission_name }}
                                            </label>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                         <label>Status: </label><span id="status_view"></span><span class="status_view"> <?php echo $status = ($role == 99 ? 'ADMIN' : ($role == 1 ? 'ACTIVE' : 'INACTIVE'))?></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                @if($role != 99)
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-primary" id="edit-roles" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                    <button type="submit" class="btn btn-primary" id="update-roles" style="display: none" title="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
            <!-- end card-body -->								
        </div>
        <!-- end card -->					
    </div>
    <!-- end col -->	
</div>
<!-- end row -->
<script src="{{ asset('/js/bootstrap3-typeahead.js') }}"></script>
<script type="text/javascript">
    if( $('#edit-roles').length > 0 ) {
        $('#edit-roles').click(function(e){
            var status_view         = $('.status_view').text();
            $('#status_view').append(''+
                                        '<select class="form-control" name="status">'+
                                            '<option value="'+status_view+'">'+status_view+'</option>'+
                                            '<option value="ACTIVE">ACTIVE</option>'+
                                            '<option value="INACTIVE">INACTIVE</option>'+
                                        '</select>'
                                        );
            $('.permissions').removeAttr('disabled', 'disabled');
            $('#role-name').removeAttr('disabled','disabled');
            $('.status_view').hide();
            $(this).hide();
            $('#update-roles').show();
        })
    }

    $(document).ready(function(){
        $('div').on('click', '#update-roles', function(e){
            swal({
                title: "Are you sure?",
                text: "Once saved, you will not be able to change your key information!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSave) => {
                if (willSave) {
                    var formData = $('#edit-roles-form').serialize();
                    $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        url: '/roles/updateRolePermissions',
                        type: "POST",
                        data: formData,
                        success: function(response) {
                            if (response != '') {
                                swal("Your account has been updated!", {
                                    icon: "success",
                                });
                                $('.get-roles-table').html(response);
                            }
                        }
                    });
                } else {
                    swal("Save Aborted");
                }
            });
            e.preventDefault();
            return false;
        })


    })
</script>