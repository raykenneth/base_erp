<table id="roles-table" class="table table-striped table-bordered table-sm">
    <thead class="thead-global">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<script type="text/javascript">
    //function in calling datatable ajax
    function getRolesTable(){
      var table = $('#roles-table').DataTable({
               ajax: {
                   url: '/roles/getAllRoles',
                   dataSrc: ''
               },
               columns: [ 
                    { data: '#' },
                    { data: 'name' },
                    { data: 'created_at' },
                    { data: 'updated_at' },
                    { data: 'status' },
                    { data: 'action' }
               ]
           });
    }
    
    $(document).ready(function(){
        //ca;; datatable ajax
        setTimeout(
          function() 
          {
            //do something special
            getRolesTable();
            //search each column datatable
            $('#roles-table thead th').each(function(e) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
            });
            $('#roles-table').DataTable().columns().every(function() {
                var that = this;
                $( 'input', this.header() ).on('keyup change', function () {
                    if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                    }
                });
            });
          }, 5000);
    })
    
</script>