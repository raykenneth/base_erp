<label>Select Function</label>
<select name="function_id" class="form-control check-approval" id="function_id">
	@foreach($functions as $key => $value)
		<option value="{{ $value->id }}">{{ $value->permission_name }}</option>
	@endforeach
</select>