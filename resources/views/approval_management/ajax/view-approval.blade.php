<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-condensed ">
		<tr>
		<td width="30px">Function</td>
		<td><b>{{$permission_name}}</b></td>
		</tr>
		<tr>
		<td>Department</td>
		<td><b>{{$department_name}}</b></td>
		</tr>
	    </table>
	    <br>
		<table class="table table-bordered table-condensed ">
			<thead>
				<tr>
					<th></th>
					<th>Preparer</th>
					<th>Checkers</th>
					<th>Approvers</th>
				</tr>
			</thead>
			<tbody>
				@if($approval)
				<?php $counter = 1; ?>
				@foreach($approval as $key => $value)
				<tr>
					<td width="30px"><?php echo $counter++ ;?></td>
					@if(isset($value['user_name'][1]))
					<td style="font-weight: bold;">&#9413{{ $value['user_name'][1] }}</td>
					@else
					<td style="font-weight: bold;">{{ $value['user_name'][2] }}</td>
					@endif
					@if(isset($value['user_checkers'][1]))
					<td style="font-weight: bold;">
						@foreach($value['user_checkers'][1] as $checker)
							<font color="orange">&#169</font>{{ $checker->last_name }}, {{ $checker->first_name }} {{ $checker->middle_name }}<br/>
						@endforeach
					</td>
					@else
					<td style="font-weight: bold;">
						@foreach($value['user_checkers'][2] as $checker)
							<font color="orange">&#169</font>{{ $checker->last_name }}, {{ $checker->first_name }} {{ $checker->middle_name }}<br/>
						@endforeach
					</td>
					@endif
					@if(isset($value['user_approvers'][2]))
					<td style="font-weight: bold;">
						@foreach($value['user_approvers'][2] as $approver)
							<font color="green">&#9398</font>{{ $approver->last_name }}, {{ $approver->first_name }} {{ $approver->middle_name }}<br/>
						@endforeach
					</td>
					@else
					<td style="font-weight: bold;">
						@foreach($value['user_approvers'][1] as $approver)
							<font color="green">&#9398</font>{{ $approver->last_name }}, {{ $approver->first_name }} {{ $approver->middle_name }}<br/>
						@endforeach
					</td>
					@endif
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
