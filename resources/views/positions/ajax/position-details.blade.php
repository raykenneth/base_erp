<form id="edit-position-form" method="post" data-parsley-validate>
<div class="row"> 
  <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
    {{ csrf_field() }}
    <input type="hidden" name="id" id="id" value="{{$position->id}}" readonly>
      <div class="form-group">
          <span id="position_name_label"></span>
          <label class="position_name_label">Name</label>
          <span id="position_name_view"></span>
          <p class="position_name_view">{{ $position->name }}</p>
      </div>
  </div>
  <button type="button" class="bt`n btn-primary" id="edit-position" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>  
  <button type="submit" class="btn btn-primary" id="update-position" style="display: none" title="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>

</div>
</form>
<script type="text/javascript">
  $(document).ready(function(){
    
        $('div').on('click', 'button#edit-position', function(e){
            $("form#edit-position-form").parsley();
            var position_name_view                   = $('.position_name_view').text();
            $('#modal-title').append('<label>Edit Position</label>')
            $('#position_name_label').append('<label>Name<span style="color:red">*</span></label>');

            $('#position_name_view').append('<input type="text" class="form-control" name="name" id="chart_of_account_name" value="'+position_name_view+'" required/>');
            $('.modal-title').hide();
            $('.position_name_label').hide();
            $('.position_name_view').hide();
            $(this).hide();
            $('#update-position').show();
            e.preventDefault();
            return false;
        })

        
       
         // Update Chart of Account form
        $('div').on('submit','form#edit-position-form',function(e){
         
          var formData = $(this).serialize();
          swal({
            title: "Are you sure?",
            text: "Once saved, you will not be able to change your key information!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willSave) => {
            if (willSave) {
              $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
              })
              $.ajax({
                 url: '/positions/updatePosition',
                 type: "POST",
                 data: formData,
                 // If success
                 success: function(response) {
                     if (response != '') {
                       $('#get-position-table').DataTable().destroy();
                        getPositionTable();
                        $('#edit-position-form').trigger("reset");
                        swal("position has been updated!", {
                          icon: "success",
                        });
                        $('#view-position-details').modal('hide');
                     }
                 },
                
              });
            } else {
              swal("Save Aborted");
            }
          });
          e.preventDefault();
          return false;
       })
  });
</script>
