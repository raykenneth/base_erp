
@extends('layouts.app')
@section('content') 
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-holder">
                    <h1 class="main-title float-left">{{ strtoupper(str_replace("-", " ", Request::segment(1))) }}</h1>
                    {{ breadCrumbs() }}
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card mb-3">
                    <div class="card-header card-header-global">
                        <h3><i class="fa fa-bullseye"></i> Positions List  <span title=" This module  "><i class=" fa fa-info-circle" style="color: #dddddd"></i></span> <span class="pull-right"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#add-position-modal"><i class="fa fa-plus" aria-hidden="true"></i></button></span></h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive get-position-table">
                            <table id="get-position-table" class="table table-bordered table-sm">
                                <thead class="thead-global">
                                    <tr>
                                        <th>#</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end card-->          
            </div>
        </div>
    </div>
    <!-- END container-fluid -->
</div>
<!-- END content -->
@include('positions.modal.add-position')
@include('positions.modal.view-position-modal')
<script type="text/javascript">
    //function in calling datatable ajax
    function getPositionTable(){
      var table = $('#get-position-table').DataTable({
               ajax: {
                   url: '/positions/getPosition',
                   dataSrc: ''
               },
               columns: [ 
                    { data: '#' },
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'action' }
               ]
           });
    }
    
    $(document).ready(function(){
        //datatable ajax
        setTimeout(
          function() 
          {
            getPositionTable();
            //search each column datatable
            $('#get-position-table thead th').each(function(e) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" class="form-control" style="font-size: 12px; font-weight: bold"/>' );
            });
            $('#get-position-table').DataTable().columns().every(function() {
                var that = this;
                $( 'input', this.header() ).on('keyup change', function () {
                    if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                    }
                });
            });
          }, 1000);
        
        //view vat type
        $('div').on('click', '.view-position-details', function(e){
            $('#view-position-details').modal('show');
            $('.position-details').html('');
            var id = this.id;
            $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
            })
            $.ajax({
               url: '/positions/getPositionDetails',
               type: "POST",
               dataType: "HTML",
               data: {id:id},
               beforeSend: function() {
                    $('.loader').show();
               },
               success: function(response) {
                   if (response != '') {
                        $('.position-details').html(response);
                   }
               },
               complete: function() {
                    $('.loader').hide();
               }
            });
            e.preventDefault();
            return false;
        });    
    })
    
</script>
@endsection