<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-header">
                <h3><i class="fa fa-user"></i> Customer Profile Details</h3>
            </div>
            <div class="card-body">
                <form id="edit-profile-form">
                    <div class="row">
                        <div class="col-lg-9 col-xl-9">
                            <div class="row">
                                <input type="hidden" name="customer_id" value="{{ $customer_details->id }}" readonly="">
                                <input type="hidden" name="company_id" value="{{ $customer_details->company_id }}" readonly="">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Company Name </label>
                                        <span id="company_name_view"></span>
                                        <p class="company_name_view">{{ $customer_details->company_name }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Company Email </label>
                                        <span id="company_email_view"></span>
                                        <p class="company_email_view">{{ $customer_details->company_email }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <span id="contact_number_view"></span>
                                        <p class="contact_number_view">{{ $customer_details->contact_number }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Contact Person</label>
                                        <span id="contact_person_view"></span>
                                        <p class="contact_person_view">{{ $unit = ( !empty($customer_details->contact_person) ? $customer_details->contact_person : 'n/a' ) }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tin Number</label>
                                        <span id="tin_number_view"></span>
                                        <p class="tin_number_view">{{ $customer_details->tin_number }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h5 style="color: gray"><i class="fa fa-map-marker" aria-hidden="true"></i> Company Address</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Unit/Bldg #</label>
                                        <span id="unit_view"></span>
                                        <p class="unit_view">{{ $unit = ( !empty($customer_details->address_number) ? $customer_details->address_number : 'n/a' ) }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Street</label>
                                        <span id="street_view"></span>
                                        <p class="street_view">{{ $unit = ( !empty($customer_details->street) ? $customer_details->street : 'n/a' ) }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Barangay</label>
                                        <span id="barangay_view"></span>
                                        <p class="barangay_view">{{ $unit = ( !empty($customer_details->barangay) ? $customer_details->barangay : 'n/a' ) }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>City</label>
                                        <span id="city_view"></span>
                                        <p class="city_view">{{ $unit = ( !empty($customer_details->city) ? $customer_details->city : 'n/a' ) }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Region</label>
                                        <span id="region_view"></span>
                                        <p class="region_view">{{ $unit = ( !empty($customer_details->region) ? $customer_details->region : 'n/a' ) }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <span id="country_view"></span>
                                        <p class="country_view">{{ $unit = ( !empty($customer_details->country) ? $customer_details->country : 'n/a' ) }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <span id="zip_code_view"></span>
                                        <p class="zip_code_view">{{ $unit = ( !empty($customer_details->zip_code) ? $customer_details->zip_code : 0 ) }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="button" class="btn btn-primary" id="edit-profile" title="Edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                    <button type="submit" class="btn btn-primary" id="update-profile" style="display: none" title="Update"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 border-left">
                            <b>Company ID: </b>
                            <br/> {{ $customer_details->company_id }}
                            <br />
                            <b>Register date: </b>
                            <br/> {{ date('M d, Y, H:i:s', strtotime($customer_details->created_at)) }}
                            <br />
                            <b>Register IP: </b>
                            <br/>
                             {{ $customer_details->registered_ip }}
                            <div class="m-b-10"></div>
                            <br />
                            <div id="avatar_image">

                                 <center>
                                     @if($customer_details->company_image == '')
                                    <img width="50%" src="{{ asset('images/profile/placeholder.jpg')}}">
                                     @else
                                     <img src="{{ asset('attachments/'.$customer_details->company_id.'/C-CP/'.$customer_details->company_image)}}">
                                    @endif
                                </center>
                                
                                <br />
                            </div>
                            <div id="image_deleted_text"></div>
                            <div class="m-b-10"></div>
                            <div class="form-group">
                                <label>{{ $customer_details->last_name.', '.$customer_details->first_name.' '.$customer_details->middle_name }}</label> 
                                <br>
                                <p><i class="fa fa-user-circle-o" aria-hidden="true"></i> {{ $customer_details->name }} <br><i class="fa fa-envelope" aria-hidden="true"></i> {{ $customer_details->email }}</p>
                                <br>
                                <label>Status: </label><span id="status_view"></span><span class="status_view"> <?php echo $status = ($customer_details->status == 1 ? 'ACTIVE' : 'INACTIVE')?></span>
                                <label>User Count: </label>
                                <span id="user_count_view"></span>
                                <span class="user_count_view">{{ $customer_details->user_count }}</span>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
            <!-- end card-body -->								
        </div>
        <!-- end card -->					
    </div>
    <!-- end col -->	
</div>
<!-- end row -->
<script type="text/javascript">

    $(document).ready(function(){
            $('#edit-profile').click(function(e){
            var company_name_view   = $('.company_name_view').text();
            var company_email_view  = $('.company_email_view').text();
            var contact_number_view = $('.contact_number_view').text();
            var tin_number_view     = $('.tin_number_view').text();
            var contact_person_view = $('.contact_person_view').text();
            var unit_view           = $('.unit_view').text();
            var street_view         = $('.street_view').text();
            var barangay_view       = $('.barangay_view').text();
            var city_view           = $('.city_view').text();
            var region_view         = $('.region_view').text();
            var country_view        = $('.country_view').text();
            var zip_code_view       = $('.zip_code_view').text();
            var status_view         = $('.status_view').text();
            var user_count_view     = $('.user_count_view').text();

            $('#company_name_view').append('<input type="text" class="form-control" name="company_name" value="'+company_name_view+'"/>');
            $('#company_email_view').append('<input type="email" class="form-control" name="company_email" value="'+company_email_view+'"/>');
            $('#contact_number_view').append('<input type="text" class="form-control" name="contact_number" value="'+contact_number_view+'"/>');
            $('#tin_number_view').append('<input type="text" class="form-control tin-mask" name="tin_number" value="'+tin_number_view+'"/>');
            $('#contact_person_view').append('<input type="text" class="form-control" name="contact_person" value="'+contact_person_view+'"/>');
            $('#unit_view').append('<input type="text" class="form-control" name="unit" value="'+unit_view+'"/>');
            $('#street_view').append('<input type="text" class="form-control" name="street" value="'+street_view+'"/>');
            $('#barangay_view').append('<input type="text" class="form-control" name="barangay" value="'+barangay_view+'"/>');
            $('#city_view').append('<input type="text" class="form-control" name="city" value="'+city_view+'"/>');
            $('#region_view').append('<input type="text" class="form-control" name="region" value="'+region_view+'"/>');
            $('#country_view').append('<input type="text" class="form-control" name="country" value="'+country_view+'"/>');
            $('#zip_code_view').append('<input type="number" class="form-control" name="zip_code" value="'+zip_code_view+'"/>');
            $('#status_view').append(''+
                                        '<select class="form-control" name="status">'+
                                            '<option value="'+status_view+'">'+status_view+'</option>'+
                                            '<option value="ACTIVE">ACTIVE</option>'+
                                            '<option value="INACTIVE">INACTIVE</option>'+
                                        '</select>'
                                        );
            $('#user_count_view').append('<input type="number" class="form-control" name="user_count" value="'+user_count_view+'"/>');


            $('.tin-mask').inputmask("999-999-999-999");
            
            $('.company_name_view').hide();
            $('.company_email_view').hide();
            $('.contact_number_view').hide();
            $('.tin_number_view').hide();
            $('.contact_person_view').hide();
            $('.unit_view').hide();
            $('.street_view').hide();
            $('.barangay_view').hide();
            $('.city_view').hide();
            $('.region_view').hide();
            $('.country_view').hide();
            $('.zip_code_view').hide();
            $('.status_view').hide();
            $('.user_count_view').hide();

            $(this).hide();
            $('#update-profile').show();
            e.preventDefault();
            return false;
        })
        $('div').on('click', '#update-profile', function(e){
            swal({
                title: "Are you sure?",
                text: "Once saved, you will not be able to change your key information!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willSave) => {
                if (willSave) {
                    var formData = $('#edit-profile-form').serialize();
                    $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        url: '/admin/customer-management/updateCustomerProfile',
                        type: "POST",
                        data: formData,
                        success: function(response) {
                            if (response != '') {
                                swal("Your account has been registered!", {
                                    icon: "success",
                                });
                                $('.get-companies-table').html(response);
                            }
                        }
                    });
                } else {
                    swal("Save Aborted");
                }
            });
            e.preventDefault();
            return false;
        })
    })
</script>