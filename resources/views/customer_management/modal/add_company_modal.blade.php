<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="add_company_modal" aria-hidden="true" id="add_company_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add new Company</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>              
            </div>
            <div class="modal-body">
                <form id="add-customer-information" novalidate>
                    <!-- SmartWizard html -->
                    <div id="smartwizard">
                        <ul>
                            <li><a href="#step-1">Step 1<br /><small>Customer Information</small></a></li>
                            <li><a href="#step-2">Step 2<br /><small>Company Information</small></a></li>
                        </ul>
                        <div>
                            <div id="step-1" class="">
                                <h3>Customer Information</h3>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>First name<span style="color:red"> *</span></label>
                                            <input class="form-control" name="first_name" type="text" id="first-name" required/>
                                            <span class="first-name"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Middle name</label>
                                            <input class="form-control" name="middle_name" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Last name<span style="color:red"> *</span></label>
                                            <input class="form-control" name="last_name" type="text" id="last-name" required />
                                            <span class="last-name"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Name Extension</label>
                                            <input class="form-control" name="name_extension" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email<span style="color:red"> *</span></label>
                                            <input class="form-control" name="email" type="email" id="email" required />
                                            <span class="email"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Username<span style="color:red"> *</span></label>
                                            <input class="form-control" name="name" type="text" id="username" required />
                                            <span class="username"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Password (required)</label>
                                            <input class="form-control" name="password" type="text" id="password" required />
                                            <span class="password"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="step-2" class="">
                                <h3>Company Information</h3>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Company Name<span style="color:red"> *</span></label>
                                            <input class="form-control" name="company_name" type="text" id="company-name" required/>
                                            <span class="company-name"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Company Email<span style="color:red"> *</span></label>
                                            <input class="form-control" name="company_email" type="text" id="company-email" required/>
                                            <span class="company-email"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Contact Person</label>
                                            <input class="form-control" name="contact_person" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Contact Number<span style="color:red"> *</span></label>
                                            <input class="form-control" name="contact_number" type="text" id="contact-number" required />
                                            <span class="contact-number"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Tin Number<span style="color:red"> *</span></label>
                                            <input class="form-control tin-mask" name="tin_number" type="text" id="tin-number" required />
                                            <span class="tin-number"></span>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                       <h5>Company Address</h5>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Unit/Bldg #/Lot Block</label>
                                            <input class="form-control" name="address_number" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Street</label>
                                            <input class="form-control" name="street" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Barangay</label>
                                            <input class="form-control" name="barangay" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input class="form-control" name="city" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Region</label>
                                            <input class="form-control" name="region" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input class="form-control" name="country" type="text"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>Zip Code</label>
                                            <input class="form-control" name="zip_code" type="text"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Company image (optional):</label> <br />
                                    <input type="file" name="company_image">
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                       <h5>Subscription Type</h5>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label>User Count (required)</label>
                                            <input class="form-control" name="user_count" type="number" id="user-count" required/>
                                            <span class="user-count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Include SmartWizard JavaScript source -->
<script type="text/javascript" src="{{ asset('js/jquery.smartWizard.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
    $('.tin-mask').inputmask("999-999-999-999");
       $('#username').blur(function(e){
         var username = $(this).val();
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          })
          e.preventDefault();
          $.ajax({
              url: '/admin/customer-management/checkCustomerUsername',
              type: "POST",
              data: {
                      username:username   
                  },
              success: function(response) {
                  $('.username').html('');
                  if (response >= 1) {
                     $('.username').append('<p style="color:red">Username already exists.</p>');
                     $('.sw-btn-next').attr('disabled', 'disabled');
                     $('.customer-finish').attr('disabled', 'disabled');
                  }else{
                     $('.sw-btn-next').removeAttr('disabled');
                     $('.customer-finish').removeAttr('disabled');
                  }
              }
          });
          e.preventDefault();
          return false;
       });
       $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
          var first_name       = $.trim($('#first-name').val());
          var last_name        = $.trim($('#last-name').val());
          var email            = $.trim($('#email').val());
          var username         = $.trim($('#username').val());
          var password         = $.trim($('#password').val());
          var first_name_error = $('.first-name').html('');
          var last_name_error  = $('.last-name').html('');
          var email_error      = $('.email').html('');
          var username_error   = $('.username').html('');
          var password_error   = $('.password').html('');
          if ( first_name == '' ) {
               $('.first-name').append('<p style="color:red">This value is required.</p>');
          }
          if ( last_name == '' ) {
               $('.last-name').append('<p style="color:red">This value is required.</p>');
          }
          if ( email == '' ) {
               $('.email').append('<p style="color:red">This value is required.</p>');
          }
          if ( username == '' ) {
               $('.username').append('<p style="color:red">This value is required.</p>');
          }
          if ( password == '' ) {
               $('.password').append('<p style="color:red">This value is required.</p>');
          }
          if ( first_name  == '' ||
               last_name   == '' ||
               email       == '' ||
               username    == '' ||
               password    == ''
            ) {
            return false;
          }else{
            if ((email.indexOf("@") == -1) && (email.indexOf(".") == -1)) {
               $('.email').append('<p style="color:red">Email format is not correct.</p>');
               return false;
            }
            if(password.length < 6){
               $('.password').append('<p style="color:red">Password must be 6 or more characters.</p>');
               return false;
            }
          }
      });
       // Toolbar extra buttons
       var btnFinish = $('<button></button>').text('Finish')
                                        .addClass('btn btn-info customer-finish')
                                        .on('click', function(e){ 
                                             var first_name       = $.trim($('#first-name').val());
                                             var last_name        = $.trim($('#last-name').val());
                                             var email            = $.trim($('#email').val());
                                             var username         = $.trim($('#username').val());
                                             var password         = $.trim($('#password').val());
                                             var company_name     = $('#company-name').val();
                                             var company_email    = $('#company-email').val();
                                             var contact_number   = $('#contact-number').val();
                                             var tin_number       = $('#tin-number').val();
                                             var user_count       = $('#user-count').val();
                                             $('.first-name').html('');
                                             $('.last-name').html('');
                                             $('.email').html('');
                                             $('.username').html('');
                                             $('.password').html('');
                                             $('.company-name').html('');
                                             $('.company-email').html('');
                                             $('.contact-number').html('');
                                             $('.tin-number').html('');
                                             $('.user-count').html('');
                                             
                                             if ( first_name == '' ) {
                                                $('.first-name').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( last_name == '' ) {
                                                $('.last-name').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( email == '' ) {
                                                $('.email').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( username == '' ) {
                                                $('.username').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( password == '' ) {
                                                $('.password').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( company_name == '' ) {
                                                 $('.company-name').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( company_email == '' ) {
                                                 $('.company-email').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( contact_number == '' ) {
                                                 $('.contact-number').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( tin_number == '' ) {
                                                 $('.tin-number').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if ( user_count == '' ) {
                                                 $('.user-count').append('<p style="color:red">This value is required.</p>');
                                             }
                                             if (  
                                                   first_name     == '' ||
                                                   last_name      == '' ||
                                                   email          == '' ||
                                                   username       == '' ||
                                                   password       == '' ||
                                                   company_name   == '' ||
                                                   company_email  == '' ||
                                                   contact_number == '' ||
                                                   tin_number     == '' ||
                                                   user_count     == ''
                                                ) {
                                                   return false;
                                                }else{
                                                   swal({
                                                     title: "Are you sure?",
                                                     text: "Once saved, you will not be able to change your key information!",
                                                     icon: "warning",
                                                     buttons: true,
                                                     dangerMode: true,
                                                   })
                                                   .then((willSave) => {
                                                     if (willSave) {
                                                        var formData = $('#add-customer-information').serialize();
                                                        $.ajaxSetup({
                                                           headers: {
                                                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                           }
                                                        })
                                                        $.ajax({
                                                           url: '/admin/customer-management/saveCustomer',
                                                           type: "POST",
                                                           data: formData,
                                                           success: function(response) {
                                                               if (response != '') {
                                                                  swal("Your account has been registered!", {
                                                                    icon: "success",
                                                                  });
                                                                  $('#smartwizard').smartWizard("reset");
                                                                  $('#add-customer-information').trigger("reset");
                                                                  $('.first-name').html('');
                                                                  $('.last-name').html('');
                                                                  $('.email').html('');
                                                                  $('.username').html('');
                                                                  $('.password').html('');
                                                                  $('.company-name').html('');
                                                                  $('.company-email').html('');
                                                                  $('.contact-number').html('');
                                                                  $('.tin-number').html('');
                                                                  $('.user-count').html('');
                                                                  $('.get-companies-table').html(response);
                                                               }
                                                           }
                                                        });
                                                     } else {
                                                        swal("Save Aborted");
                                                     }
                                                   });
                                                }
                                             e.preventDefault();
                                             return false;
                                         });
       var btnCancel = $('<button></button>').text('Cancel')
                                        .addClass('btn btn-danger')
                                        .on('click', function(){ 
                                          $('#smartwizard').smartWizard("reset");
                                          $('#add-customer-information').trigger("reset");
                                          $('.first-name').html('');
                                          $('.last-name').html('');
                                          $('.email').html('');
                                          $('.username').html('');
                                          $('.password').html('');
                                          $('.company-name').html('');
                                          $('.company-email').html('');
                                          $('.contact-number').html('');
                                          $('.tin-number').html('');
                                          $('.user-count').html('');
                                       });
    
       // Smart Wizard
       $('#smartwizard').smartWizard({
               selected: 0,
               theme: 'arrows',
               transitionEffect:'fade',
               toolbarSettings: {toolbarPosition: 'bottom',
                                 toolbarExtraButtons: [btnFinish, btnCancel]
                               }
            });
    });
</script>