<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class CustomerModel extends Model
{
	//get chart of account group
	public static function getCustomers()
	{
		return DB::table('customers')
				->where('company_id', Auth::user()->company_id)
				->get()->toArray();
	}

	public static function checkifCustomerCodeExists($data)
	{
		return DB::table('customers')
				->where('company_id', Auth::user()->company_id)
				->where('code', $data)
				->count();
	}


	public static function addCustomer($data)
	{
		DB::table('customers')
				->insert([
					'code' 					=> $data['code'],
					'name' 					=> $data['name'],
					'bank_name' 			=> $data['bank_name'],
					'bank_account_number'	=> $data['bank_account_number'],
					'expense_account'		=> $data['expense_account'],
					'customer_type_id' 		=> $data['customer_type_id'],
					'remarks'				=> $data['remarks'],
					'address_street' 		=> $data['address_street'],
					'address_city'			=> $data['address_city'],
					'address_province' 		=> $data['address_province'],
					'address_zip' 			=> $data['address_zip'],
					'address_country' 		=> $data['address_country'],
					'contact_mobile' 		=> $data['contact_mobile'],
					'contact_landline' 		=> $data['contact_landline'],
					'contact_email' 		=> $data['contact_email'],
					'contact_website' 		=> $data['contact_website'],
					'contact_fax'			=> $data['contact_fax'],
					'contact_person' 		=> $data['contact_person'],
					'contact_role' 			=> $data['contact_role'],
					'tin' 					=> $data['tin'],
					'vatable' 				=> $data['vatable'],
					'credit_limit'			=> $data['credit_limit'],
					'term' 					=> $data['term'],
					'created_at'			=> date('Y-m-d H:i:s'),
					'company_id' 			=> Auth::user()->company_id
				]);
		return DB::getPdo()->lastInsertId();
	}
	public static function updateCustomer($data)
	{
		DB::table('customers')
				->where('id',$data['id'])
				->update([
					'code' 					=>$data['customer_code'],
					'name' 					=> $data['customer_name'],
					'bank_name' 			=> $data['customer_bank_name'],
					'bank_account_number'	=> $data['customer_bank_account'],
					'expense_account'		=> $data['expense_account2'],
					'customer_type_id' 		=> $data['customer_type2'],
					'remarks'				=> $data['remarks'],
					'address_street' 		=> $data['customer_street'],
					'address_city'			=> $data['customer_city'],
					'address_province' 		=> $data['customer_province'],
					'address_zip' 			=> $data['customer_zip_code'],
					'address_country' 		=> $data['customer_country'],
					'contact_mobile' 		=> $data['customer_mobile'],
					'contact_landline' 		=> $data['customer_landline'],
					'contact_email' 		=> $data['customer_email'],
					'contact_website' 		=> $data['customer_website'],
					'contact_fax'			=> $data['customer_fax'],
					'contact_person' 		=> $data['customer_contact_person'],
					'contact_role' 			=> $data['customer_contact_role'],
					'tin' 					=> $data['customer_tin'],
					'vatable' 				=> $data['customer_vatable'],
					'credit_limit'			=> $data['customer_credit_limit'],
					'status'				=> $data['customer_status'],
					'term' 					=> $data['customer_term'],
					'updated_at'			=> date('Y-m-d H:i:s'),
					'company_id' 			=> Auth::user()->company_id
				]);
		return DB::table('customers')
				->where('id', $data['id'])
				->first();
	}

	public static function getCustomerDetails($data)
	{
		return DB::table('customers')
				->where('id', $data)
				->first();
	}
	public static function getActiveCustomers()
	{
		return DB::table('customers')
				->where('company_id', Auth::user()->company_id)
				->where('status', '1')
				->where('customer_type_id', '!=', 0)
				->orderBy('name')
				->get()->toArray();
	}
	public static function getCustomerActiveOrderByCode()
	{
		return DB::table('customers')
				->where('company_id', Auth::user()->company_id)
				->where('status', '1')
				->where('customer_type_id', '!=', 0)
				->orderBy('code')
				->get()->toArray();
	}
	public static function getCustomerTerms($data)
	{
		return DB::table('customers')
				->where('id', $data)
				->where('company_id', Auth::user()->company_id)
				->where('status', '1')
				->first();
	}
	public static function modulePDF($data)
	{
		return DB::table('module_pdf')
				->insert([$data]);
	}
	public static function updateUI($data)
	{
		$update = DB::table('module_pdf')
				->where('function_id',$data['function_id'])
				->update([
					'color' => $data['color']
				]);

		return DB::table('module_pdf')
				->select('id')
				->where('function_id',$data['function_id'])
				->first();
	}
	public static function getUISettings()
	{
		return DB::table('module_pdf')
				->where('company_id',Auth::user()->company_id)
				->get();
	}
	public static function getColor($id)
	{
		return DB::table('module_pdf')
			->select('color')
			->where('function_id',$id)
			->first();
	}
	public static function addColor($id)
	{
		$result = DB::table('module_pdf')
			->insert([
					'function_id' 		=> $id,
					'color' 			=> '008080',
					'company_id' 		=> Auth::user()->company_id
 				]);
		$last_id = DB::getPdo()->lastInsertId();

		return DB::table('module_pdf')
			->select('color')
			->where('id',$last_id)
			->first();

	}
	public static function getExistingFiles($transaction_id,$inventory_type)
	{
		return DB::table('attachment_files')
				->where('number_id',$transaction_id)
				->where('inventory_type',$inventory_type)
				->where('company_id',Auth::user()->company_id)
				->get()->toArray();

	}
	public static function deleteAttachment($id)
	{
		return DB::table('attachment_files')
				->where('id',$id)
				->delete();

	}
	public static function getFileName($id)
	{
		return DB::table('attachment_files')
					->select('filename')
					->where('id',$id)
					->first();
	}

	public static function getCustomerID($code, $company_id)
	{
		return DB::table('customers')
				->where('code', $code)
				->where('company_id', $company_id)
				->first();
	}
	public static function getClosingofBooks()
	{
		return DB::table('company')
				->select('closing_of_book')
				->where('id',Auth::user()->company_id)
				->first();
	}
	public static function assignedClosingOfBooks($data)
	{
		return DB::table('company')
				->where('id',Auth::user()->company_id)
				->update([
					'closing_of_book' => $data['closing_of_books']
				]);
		

	}
	public static function getCustomerLimit($term,$skip,$count)
	{
	    if ($term) {
	        $code = DB::table('customers')
	                ->select('code')
	                ->where('code', 'LIKE', '%' .$term. '%')
	                ->first();
	        if(isset($code->code))
	        {
	            return DB::table('customers')
	                ->select(DB::raw('
	                    id,
	                    CONCAT(code," : ",name) AS text
	                '))
	                // ->where('code', 'LIKE', '%' . $term. '%')
	                // ->orWhere('name', 'LIKE',  '%' . $term. '%')
	                ->where('code', 'LIKE', '%' .$term. '%')
	                ->where('status', '1')
	                ->where('company_id', Auth::user()->company_id)
	                ->skip($skip)
	                ->take($count)
	                ->get()->toArray();
	        } 
	        else
	        {
	            return DB::table('customers')
	                ->select(DB::raw('
	                    id,
	                    CONCAT(code," : ",name) AS text
	                '))
	                // ->where('code', 'LIKE', '%' . $term. '%')
	                // ->orWhere('name', 'LIKE',  '%' . $term. '%')
	                ->where('name', 'LIKE', '%' .$term. '%')
	                ->where('status', '1')
	                ->where('company_id', Auth::user()->company_id)
	                ->skip($skip)
	                ->take($count)
	                ->get()->toArray(); 
	        }
	    }else{
	        return DB::table('customers')
	                ->select(DB::raw('
	                    id,
	                    CONCAT(code," : ",name) AS text
	                '))
	                // ->where('code', 'LIKE', '%' . $term. '%')
	                // ->orWhere('name', 'LIKE',  '%' . $term. '%')
	                ->where('name', 'LIKE', '%' .$term. '%')
	                ->where('status', '1')
	                ->where('company_id', Auth::user()->company_id)
	                ->skip($skip)
	                ->take($count)
	                ->get()->toArray(); 
	    }
	}
	public static function countCustomer()
	{
	    return DB::table('customers')
	            ->where('status', '1')
	            ->where('company_id', Auth::user()->company_id)
	            ->orderBy('code','ASC')
	            ->count();
	}
}
