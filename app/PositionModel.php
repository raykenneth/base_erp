<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class PositionModel extends Model
{
    public static function getPosition()
    {
        return DB::table('positions')
                ->where('company_id', Auth::user()->company_id)
                ->orWhere('company_id', 0)
                ->orderBy('id')
                ->get()->toArray();
    }

    public static function savePosition($data)
    {
        $check = DB::table('positions')
                    ->where('name', $data)
                    ->get();
        if ($check->count() >= 1) {
            $id = DB::table('positions')
                    ->select('id')
                    ->where('name', $data)
                    ->get();
            return $id[0]->id;
        }else{
            DB::table('positions')
                ->insert([
                    'name'        => $data,
                    'company_id'  => Auth::user()->company_id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'created_by'  => Auth::user()->id
                ]);
            return DB::getPdo()->lastInsertId();
        }
    }
    public static function getPositionDetails($id)
    {
        return DB::table('positions')
                ->where('id',$id)
                ->first();
    }
    public static function addPosition($data)
    {
        $insert =  DB::table('positions')
                ->insert([
                    'name'        => $data['name'],
                    'company_id'  => Auth::user()->company_id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'created_by'  => Auth::user()->id
                ]);
        return DB::getPdo()->lastInsertId();
    }
    public static function updatePosition($data)
    {
        return DB::table('positions')
                ->where('id',$data['id'])
                ->update([
                    'name'        => $data['name'],
                    'company_id'  => Auth::user()->company_id,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'created_by'  => Auth::user()->id
                ]);
    }
}
