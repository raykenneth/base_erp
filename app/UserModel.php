<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class UserModel extends Model
{
	//get all companies
	public static function getAllCompanies()
	{
		return DB::table('company')
				->get();
	}
	//check customer username
    public static function checkCustomerUsername($data)
    {
    	return DB::table('users')
    			->where('name', $data['username'])
    			->get();
    }
    //save customer information data
    public static function saveCustomerData($data)
    {
    	return DB::table('users')
    			->insert($data);


    }
    //save customer company information data
    public static function saveCustomerCompanyData($data)
    {
    	       DB::table('company')
    			->insert($data);
   		return DB::getPdo()->lastInsertId();
    	
    }
    //get single customer details
    public static function getCustomerDetails($data)
    {
    	return DB::table('company as c')
    			->leftJoin('users as u', 'c.id', '=', 'u.company_id')
    			->where('c.id', $data['id'])
    			->where('u.role_id', '1')
    			->get();
    }
    //update customer information
    public static function updateCustomerProfile($data)
    {
        DB::table('company')
            ->where('id', $data['company_id'])
            ->update([
                'company_name'      => $data['company_name'],
                'company_email'     => $data['company_email'],
                'contact_number'    => $data['contact_number'],
                'tin_number'        => $data['tin_number'],
                'contact_person'    => $data['contact_person'],
                'address_number'    => $data['unit'],
                'street'            => $data['street'],
                'barangay'          => $data['barangay'],
                'city'              => $data['city'],
                'region'            => $data['region'],
                'country'           => $data['country'],
                'zip_code'          => $data['zip_code'],
                'status'            => $status = ( $data['status'] == 'ACTIVE' ? 1 : 0 ),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);  

        return DB::table('users')
            ->where('id', $data['customer_id'])
            ->update([
                'status'            => $status = ( $data['status'] == 'ACTIVE' ? 1 : 0 ),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);
    }

    public static function getAllUsers()
    {
        return DB::table('users')
                ->where('company_id', Auth::user()->company_id)
                ->where('role_id', '!=', 2)
                ->get();
    }

    /////actual users of each company
    public static function getActualUserCount($company_id)
    {
        return DB::table('users')
                ->where('company_id',$company_id)
                ->count();
    }

    ////user_count field for the company
    public static function getAssignedUserCount($company_id)
    {
        return DB::table('company')
                ->select('user_count')
                ->where('id',$company_id)
                ->first();
    }

    // Get the user details

    public static function getMyProfileDetails()
    {
        return DB::table('users as u')
                ->select('u.first_name', 'u.middle_name', 'u.last_name', 'u.name_extension as name_extension', 'u.name as name', 'u.status as status', 'u.email', 'u.image', 'u.registered_ip', 'u.created_at as registered_at', 'r.name as role_name', 'u.role_id as role_id', 'u.id as id', 'u.company_id as company_id', 'u.created_at as created_at','u.updated_at as updated_at')
                ->where('u.id',userId())
                ->leftJoin('user_roles as r', 'u.role_id', '=', 'r.id')
                ->first();
    }

    // Update my profile details

    public static function updateMyProfile($data)
    {
        return DB::table('users')
                ->where('id',userId())
                ->update($data);
    }
    public static function updateCompanyLogo($data)
    {
        return DB::table('company')
                ->where('id',$data['company_id'])
                ->update([
                        'company_image'     => $data['imagename'],
                        'updated_at'        => date('Y-m-d H:i:s')
                ]);
    }
    public static function updateProfilePicture($data)
    {
        return DB::table('users')
                ->where('id',$data['id'])
                ->update([
                        'image'             => $data['imagename'],
                        'updated_at'        => date('Y-m-d H:i:s')
                ]);
    }
    public static function deleteCompanyLogo($id,$image)
    {
        return DB::table('company')
                ->where('id',$id)
                ->update([
                        'company_image'     => '',
                ]);
    }
    public static function deleteProfilePicture($id,$image)
    {
        return DB::table('users')
                ->where('id',$id)
                ->update([
                        'image'     => '',
                ]);
    }

    public static function checkUsername($data)
    {
        return DB::table('users')
                ->where('name', $data['username'])
                ->get();
    }

    public static function getUserRoles()
    {
        return DB::table('users as u')
                ->select('u.last_name', 'u.first_name', 'u.middle_name', 'r.name as role_name', 'r.id as role_id')
                ->leftJoin('user_roles as r', 'u.role_id', '=', 'r.id')
                ->where('u.company_id', Auth::user()->company_id)
                ->get();
    }

    public static function saveUserInformation($data)
    {
        return DB::table('users')
                ->insert($data);
    }

    //get single user details
    public static function getUserDetails($data)
    {
        return DB::table('users as u')
                ->select('u.first_name', 'u.middle_name', 'u.last_name', 'u.name_extension as name_extension', 'u.name as username', 'u.status as status', 'u.email', 'u.image', 'u.registered_ip', 'u.created_at as registered_at', 'r.name as role_name', 'p.name as position_name', 'd.name as department_name', 'u.role_id as role_id', 'u.id as id', 'u.company_id as company_id', 'u.created_at as created_at','u.updated_at as updated_at')
                ->leftJoin('user_roles as r', 'u.role_id', '=', 'r.id')
                ->leftJoin('positions as p', 'u.position_id', '=', 'p.id')
                ->leftJoin('departments as d', 'u.department_id', '=', 'd.id')
                ->where('u.id', $data['id'])
                ->get();
    }

    public static function updateUserDetails($data)
    {
        return  DB::table('users')
                ->where('id', $data['user_id'])
                ->update([
                    'first_name'    => $data['first_name'],
                    'middle_name'   => $data['middle_name'],
                    'last_name'     => $data['last_name'],
                    'name_extension'=> $data['name_extension'],
                    'name'          => $data['username'],
                    'email'         => $data['email'],
                    'role_id'       => $data['role_id'],
                    'updated_at'    => date('Y-m-d H:i:s'),
                    'updated_by'    => Auth::user()->id,
                    'status'        => ( $data['status'] == 'ACTIVE' ? 1 : 0 )
                ]);
    }

    public static function updateUserDepartment($data)
    {
        $check = DB::table('departments')
                    ->where('name', $data['department_name'])
                    ->where('company_id', Auth::user()->company_id)
                    ->get();
        if ($check->count() >= 1) {
            return DB::table('users')
                    ->where('id', $data['user_id'])
                    ->update([
                        'department_id' => $check[0]->id
                    ]);
        }else{
            DB::table('departments')
                ->insert([
                    'name'          => $data['department_name'],
                    'company_id'    => Auth::user()->company_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'created_by'    => Auth::user()->id
                ]);
            $department_id = DB::getPdo()->lastInsertId();
            return DB::table('users')
                    ->where('id', $data['user_id'])
                    ->update([
                        'department_id' => $department_id
                    ]);
        }
    }  

    public static function updateUserPosition($data)
    {
        $check = DB::table('positions')
                    ->where('name', $data['position_name'])
                    ->where('company_id', Auth::user()->company_id)
                    ->get();
        if ($check->count() >= 1) {
            return DB::table('users')
                    ->where('id', $data['user_id'])
                    ->update([
                        'position_id' => $check[0]->id
                    ]);
        }else{
            DB::table('positions')
                ->insert([
                    'name'          => $data['position_name'],
                    'company_id'    => Auth::user()->company_id,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'created_by'    => Auth::user()->id
                ]);
            $position_id = DB::getPdo()->lastInsertId();
            return DB::table('users')
                    ->where('id', $data['user_id'])
                    ->update([
                        'position_id' => $position_id
                    ]);
        }
    }  
    public static function getCompanyDetails()
    {
        return DB::table('users as u')
                ->leftJoin('company as c', 'u.company_id','=','c.id')
                ->where('u.company_id',Auth::user()->company_id)
                ->first();

    }
    public static function updateCompanyProfile($data)
    {
        return DB::table('company')
                ->where('id',$data['id'])
                ->update([
                'company_name'              => $data['name'],
                'company_email'             => $data['email'],
                'contact_number'            => $data['contact_number'],
                'tin_number'                => $data['tin_number'],
                'contact_person'            => $data['contact_person'],
                'fax_number'                => $data['fax'],
                'street'                    => $data['shipping_street'],
                'city'                      => $data['shipping_city'],
                'region'                    => $data['shipping_province'],
                'country'                   => $data['shipping_country'],
                'zip_code'                  => $data['shipping_zip_code'],
                'billing_street'            => $data['billing_street'],
                'billing_city'              => $data['billing_city'],
                'billing_region'            => $data['billing_province'],
                'billing_country'           => $data['billing_country'],
                'billing_zip_code'          => $data['billing_zip_code'],
                'updated_at'                => date('Y-m-d H:i:s')
            ]);  
                
    }

    public static function getCompanyLogo()
    {
        return DB::table('company')
                ->select('company_image')
                ->where('id',\Auth::user()->company_id)
                ->first();
    }

    public static function getProfilePicture()
    {
        return DB::table('users')
                ->select('image')
                ->where('id',\Auth::user()->id)
                ->first();
    }

    public static function getUserData($data)
    {
        return DB::table('users')
                ->where('id', $data)
                ->first();
    }
}
