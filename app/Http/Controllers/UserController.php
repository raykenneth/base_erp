<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\UserModel;
use App\DepartmentModel;
use App\PositionModel;
use App\RoleModel;
use App\ApprovalModel;
use DB;
use File;
use Illuminate\Support\Facades\Validator;
use Image;
use Session;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->status == 1) {
            $data['actual_usercount']     = UserModel::getActualUserCount(Auth::user()->company_id);
            $data['assigned_usercount']   = UserModel::getAssignedUserCount(Auth::user()->company_id);
            return view('user.index',$data);
        }else{
            return redirect('/inactive');
        }
    }

    public function getAllUsers()
    {
        $counter                = 1;
        $data                   = UserModel::getAllUsers();
        foreach ($data as $key => $value) {
            $department             = DepartmentModel::getDepartmentDetails($value->department_id); 
            $department_name        = ($department) ? $department->name : 'Unassigned'; 
            $role                   = RoleModel::getUserRoleName($value->role_id);
            $role_name              = ($role) ? $role[0]->name : 'Unassigned'; 
            $position               = PositionModel::getPositionDetails($value->position_id);
            $position_name          = ($position) ? $position->name : 'Unassigned'; 
            $result[]   = array(
                '#'                     => '<span style="font-size: 12px; color: gray">'.$counter++.'</span>',
                'name'                  => '<p>'.$value->last_name.', '.$value->first_name.' '.$value->middle_name.'</p>',
                'username'              => '<p>'.$value->name.'</p>',
                'email'                 => '<p>'.$value->email.'</p>',
                'department'            => '<p>'.$department_name.'</p>',
                'position'              => '<p>'.$position_name.'</p>',
                'role'                  => '<p>'.$role_name.'</p>',
                'actions'               => '<button class="btn btn-primary btn-sm view-user-details" id="'.$value->id.'" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></button>',
                'status'                => $value->status == 1 ? 'ACTIVE' : 'INACTIVE'
            );
        }
        return response()->json($result);
    }

    public function checkUsername(Request $request)
    {
        $data   = $request->all();
        $result = UserModel::checkUsername($data);
        return response()->json($result->count());
    }

    public function saveUserInformation(Request $request)
    {   
        $data           = $request->all();
        $position_id    = PositionModel::savePosition($data['position']);
        $department_id  = DepartmentModel::saveDepartment($data['department']);
        $array          = array(
                            'first_name'        => $data['first_name'], 
                            'middle_name'       => $data['middle_name'], 
                            'last_name'         => $data['last_name'], 
                            'name_extension'    => $data['name_extension'], 
                            'email'             => $data['email'], 
                            'name'              => $data['name'], 
                            'password'          => bcrypt($data['password']), 
                            'role_id'           => $data['role_id'], 
                            'position_id'       => $position_id,
                            'department_id'     => $department_id,
                            'company_id'        => Auth::user()->company_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                            'created_by'        => Auth::user()->id,
                            'registered_ip'     => $request->ip(),
                            'status'            => 1
                        );
        $result         = UserModel::saveUserInformation($array);
        $returnHTML     = view('user.ajax.users-table');
        echo $returnHTML;
    }

    public function getUserDetails(Request $request)
    {
        $data                   = $request->all();
        $result                 = UserModel::getUserDetails($data);
        $roles                  = RoleModel::getAllRoles();
        $all_department         = DepartmentModel::getAllDepartmentExceptOwnDepartment($result[0]->department_id);
        $get_departments        = DepartmentModel::pr_department_assigned($result[0]->id);
        // User Role Permission List
        $all_role_permission    = userPermissions();
        // Get Role Permission
        $get_role_permission    = $this->getUserPermission($all_role_permission);
        // Get My View Mode
        $get_view_mode          = UserModel::getViewMode($result[0]->id);
        if($get_departments)
        {
            $separte_department    = explode(',', $get_departments[0]->dept_id);
            $pr_departments        = $this->getDepartmentDetails($separte_department);
            foreach($pr_departments as $key => $value)
            {
                $pr_dept_id[] = $value->details->id;
            }
        }
        else
        {
            $pr_departments = [];
            $pr_dept_id     = [];
        }
        if($get_view_mode->view_mode_modules)
        {
            $separte_view_mode    = explode(',', $get_view_mode->view_mode_modules);
            $view_mode            = $this->getUserPermission($separte_view_mode);
            foreach($view_mode as $key => $value)
            {
                $module_id[] = $value->id;
            }
        }
        else
        {
            $view_mode = [];
            $module_id = [];
        }
        $returnHTML = view('user.ajax.user-details',
            [
                'user_details'          => $result[0],
                'role_details'          => $roles,
                'department_details'    => $all_department,
                'pr_departments'        => $pr_departments,
                'pr_dept_id'            => $pr_dept_id,
                'all_role_permission'   => $get_role_permission,
                'view_mode'             => $view_mode,
                'module_id'             => $module_id
            ])->render();
        echo $returnHTML;
    }

    private function getUserPermission($role_permission)
    {
        $data = [];
        foreach($role_permission as $key => $role)
        {
            $items                      = new \stdClass();
            $items->id                  = $role;
            $items->role_permission     = RoleModel::userRolesPermissionNameWithoutReports($role);
            array_push($data, $items); 
        }
        return $data;
    }

    public function updateUserDetails(Request $request)
    {
        DB::transaction( function(&$data) use ($request) {
            $result     = UserModel::updateUserDetails($request);
            $data       = $request->all();   
            if ($result) {
                $department_id  =   DepartmentModel::getDepartmentID($request);
                // if it has value
                if(isset($data['pr_department']))
                {
                    $departments = implode(",", $data['pr_department']);
                    DepartmentModel::assignedPRDepartment($data,$departments);
                   
                }
                else
                {
                    DepartmentModel::nonAssignedDepart($data['user_id'],$data['company_id']);
                }

                if(isset($data['allowed_access']))
                {
                    $modules = implode(",", $data['allowed_access']);
                    UserModel::updateViewMode($data,$modules);
                }
                $checkers       =   ApprovalModel::getCheckerApprovalsByDepartment($request, ($department_id) ? $department_id->id : '');
                $approvers      =   ApprovalModel::getApproverApprovalsByDepartment($request, ($department_id) ? $department_id->id : '');
                if (isset($checkers)) {
                    foreach ($checkers as $c => $checker) {
                        ApprovalModel::deleteAllWithDifferentDepartment($checker->id, 'checker_approval_table');
                    }
                }
                if (isset($approvers)) {
                    foreach ($approvers as $c => $approver) {
                        ApprovalModel::deleteAllWithDifferentDepartment($approver->id, 'approver_approval_table');
                    }
                }
                UserModel::updateUserDepartment($request);
                UserModel::updateUserPosition($request);
                $returnHTML     = view('user.ajax.users-table');
                echo $returnHTML;
            }else{
                $returnHTML     = view('user.ajax.users-table');
                echo $returnHTML;
            }
        });       
    }

    // Edit My Profile 

    public function myProfile()
    {
        // get the user details
        $data['user_details'] = UserModel::getMyProfileDetails();
        $data['department']   =DepartmentModel::getDepartmentDetails($data['user_details']->department_id);
        return view('my_profile.index',$data);
    }


    // Submit edit myProfile form
    public function updateMyProfile(Request $request)
    {
      $data = $request->all();
      $user   = array(
                    'first_name'        => ucwords($data['first_name']), 
                    'middle_name'       => ucwords($data['middle_name']), 
                    'last_name'         => ucwords($data['last_name']), 
                    'name_extension'    => ucwords($data['name_extension']), 
                    'email'             => $data['email'], 
                    'updated_at'        => date('Y-m-d H:i:s')
                );
        $result = UserModel::updateMyProfile($user);
        // Always get the latest value.
        $updated_details = UserModel::getMyProfileDetails();
        return response()->json($updated_details);
    }
    public function updateProfilePicture(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
        ]);

        if ($validator->fails()) {
            Session::flash('error', 'Error!');
            return response()->json(0);
        }
        else{
        $data['company_id'] = Auth::user()->company_id;
        $data['id'] = Auth::user()->id;
        $image = $request->file('image');
        $data['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path().'/attachments/'.$data['company_id'].'/C-CP/';
        if (!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
        }
        $img = Image::make($image);
        $img->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$data['imagename']);
        $result = UserModel::updateProfilePicture($data);
        return response()->json($result);
    
        }
    }
    public function deleteProfilePicture($id){
        $image = profilePicture();
        $company_id = Auth::user()->company_id;
        $destinationPath = public_path().'/attachments/'.$company_id.'/C-CP/';
        File::delete($destinationPath .'/'.$image);
        $result = UserModel::deleteProfilePicture($id,$image);
        return back();
    }

    //Signature
    public function updateProfileSignature(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'signature' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
        ]);

        if ($validator->fails()) {
            Session::flash('error', 'Error!');
            return response()->json(0);
        }
        else{
        $data['company_id'] = Auth::user()->company_id;
        $data['id'] = Auth::user()->id;
        $signature = $request->file('signature');
        $data['imagename'] = time().'.'.$signature->getClientOriginalExtension();
        $destinationPath = public_path().'/attachments/'.$data['company_id'].'/C-CP/';
        if (!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, 0777, true, true);
        }
        $img = Image::make($signature);
        $img->resize(300, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$data['imagename']);
        $result = UserModel::updateProfileSignature($data);
        return response()->json($result);
    
        }
    }
    public function deleteProfileSignature($id){
        $signature = profileSignature();
        $company_id = Auth::user()->company_id;
        $destinationPath = public_path().'/attachments/'.$company_id.'/C-CP/';
        File::delete($destinationPath .'/'.$signature);
        $result = UserModel::deleteProfileSignature($id,$signature);
        return back();
    }


    
    public function checkUsernameProfile(Request $request)
    {
        $data   = $request->all();
        $result = UserModel::checkUsername($data);
        return response()->json($result->count());
    }
    public function getMonths()
    {
        $result = UserModel::getMonths();
        return response()->json($result);
    }
    private function getDepartmentDetails($separte_department)
    {
        $data = [];
        foreach($separte_department as $value)
        {
            $department                      = new \stdClass();
            $department->details             = DepartmentModel::getDepartmentDetails($value); 
            array_push($data, $department);
        }
        return $data;
    }
}
