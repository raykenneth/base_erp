<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\RoleModel;
use App\UserModel;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::user()->status == 1) {
            $data['permissions'] = RoleModel::getAllPermissions();
            return view('role.index', $data);
        }else{
            return redirect('/inactive');
        }
    }

    public function getUserRoles()
    {
        $users   = UserModel::getUserRoles();
        foreach ($users as $key => $value) {
            $result[] = array(
                        'user'      => $value->last_name.', '.$value->first_name.' '.$value->middle_name,
                        'role'      => $value->role_name,
                        'option'    => '<button class="btn btn-success choose-role" id="'.$value->role_id.'" data-id="'.$value->role_name.'"><i class="fa fa-check" aria-hidden="true"></i></button>'
                    );
        }
        return response()->json($result);
    }

    public function getAllRoles()
    {
        $counter    = 1;
        $roles      = RoleModel::getAllRoles();
        foreach ($roles as $key => $value) {
            $result[] = array(
                '#'                     => '<span style="font-size: 12px; color: gray">'.$counter++.'</span>',
                'name'                  => '<p>'.$value->name.'</p>',
                'created_at'            => '<p>'.$value->created_at.'</p>',
                'updated_at'            => '<p>'.$value->updated_at.'</p>',
                'action'               => '<button class="btn btn-primary btn-sm view-role-details" id="'.$value->id.'" title="View"><i class="tiny material-icons">visibility</i></button>',
                'status'                => $value->status == 1 || $value->status == 99 ? 'ACTIVE' : 'INACTIVE'
                    );
        }
        return response()->json($result);
    }

    public function getRoleDetails(Request $request)
    {
        $data       = $request->all();
        $result     = RoleModel::getRoleDetails($data);
        foreach ($result[1] as $key => $value) {
            $array[]  = $value->id;
        }
        $permission = RoleModel::getAllPermissions();
        $role_name  = RoleModel::getRoleName($data);
        $returnHTML = view('role.ajax.role-details',
            [
                'role_details'       => $array,
                'role_id'            => $data['id'],
                'role'               => $result[0][0]->status,
                'permission'         => $permission,
                'role_name'          => $role_name[0]->name
            ])->render();
        echo $returnHTML;
    }

    public function updateRolePermissions(Request $request)
    {
        $data       =   $request->all();
        $permission =   implode(",", $data['permissions']);
        $result     =   RoleModel::updateRolePermissions($data, $permission);
        $returnHTML     = view('role.ajax.roles-table');
        echo $returnHTML;
    }

    public function saveRoleInformation(Request $request)
    {
        $data       = $request->all();
        $permission =   implode(",", $data['permissions']);
        $result     =   RoleModel::addRoleInformation($data, $permission);
        $returnHTML     = view('role.ajax.roles-table');
        echo $returnHTML;
    }
}
