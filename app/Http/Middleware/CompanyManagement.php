<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use App\RoleModel;

class CompanyManagement
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // generate parmission
        $user_permissions = RoleModel::getPermissions( array( 'user_id' => Auth::user()->id ) );
         
        $permission_keys = explode(",", $user_permissions[0]->permission);

        if(( in_array(1, $permission_keys) )){
            return $next($request);
        } 
        // If every condition fails
        return redirect('/inactive');
    }
}