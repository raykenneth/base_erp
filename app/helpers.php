 <?php

use App\RoleModel;
use App\AuditTrailModel;
use App\UserModel;
use App\NotificationModel;
use App\GlobalModel;

function userId()
{   
    if (!auth()->guest()){
        return auth()->user()->id;
    }else{
        return false;
    }
}

function roleId()
{
    if (!auth()->guest()){
        return auth()->user()->role_id;
    }else{
        return false;
    }
}

function userRolesPermission($number)
{
	$role_permission = RoleModel::userRolesPermission($number);

	return (string)$role_permission[0]->id;
}

function userPermissions()
{
	$permission = RoleModel::userPermissions(userId(), roleId());

	return explode(',', $permission[0]->permission);
}

function breadCrumbs()
{
	$root 		= url('/');	
	$segment1	= Request::segment(1);
	$segment2   = Request::segment(2);
	$segment3 	= Request::segment(3);
	$segment4   = Request::segment(4);
	if ($segment4 && $segment3 && $segment2 && $segment1) {
		echo '<ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="'.$root.'">Home</a></li>
                        <li class="breadcrumb-item"><a href="'.$root.'/'.$segment1.'">'.ucwords(str_replace("-", " ", $segment1)).'</a></li>
                        <li class="breadcrumb-item"><a href="'.$root.'/'.$segment1.'/'.$segment2.'">'.ucwords(str_replace("-", " ", $segment2)).'</a></li>
                        <li class="breadcrumb-item"><a href="'.$root.'/'.$segment1.'/'.$segment2.'/'.$segment3.'">'.ucwords(str_replace("-", " ", $segment3)).'</a></li>
                        <li class="breadcrumb-item active">'.ucwords(str_replace("-", " ", $segment4)).'</li>
                    </ol>';
	}elseif ($segment3 && $segment2 && $segment1) {
		echo '<ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="'.$root.'">Home</a></li>
                        <li class="breadcrumb-item"><a href="'.$root.'/'.$segment1.'">'.ucwords(str_replace("-", " ", $segment1)).'</a></li>
                        <li class="breadcrumb-item"><a href="'.$root.'/'.$segment1.'/'.$segment2.'">'.ucwords(str_replace("-", " ", $segment2)).'</a></li>
                        <li class="breadcrumb-item active">'.ucwords(str_replace("-", " ", $segment3)).'</li>
                    </ol>';
	}elseif ($segment2 && $segment1) {
		echo '<ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="'.$root.'">Home</a></li>
                        <li class="breadcrumb-item"><a href="'.$root.'/'.$segment1.'">'.ucwords(str_replace("-", " ", $segment1)).'</a></li>
                        <li class="breadcrumb-item active">'.ucwords(str_replace("-", " ", $segment2)).'</li>
                    </ol>';
	}elseif ($segment1) {
		echo '<ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="'.$root.'">Home</a></li>
                        <li class="breadcrumb-item active">'.ucwords(str_replace("-", " ", $segment1)).'</li>
                    </ol>';
	}

}

function trailCreate($data)
{
    return AuditTrailModel::trailCreate($data);
}

function trailUpdate($data)
{
    return AuditTrailModel::trailUpdate($data);
}

function trailDelete($data)
{
    return AuditTrailModel::trailDelete($data);
}

function convert_number_to_words($integer) 
{
    $orig_number  = explode('.',$integer);
    $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);
    $whole_number = $f->format($orig_number[0]);
    if(count($orig_number) == 1):
        $decimal_number = '00/100';
    else:
        $decimal_number = str_pad($orig_number[1], 2, '0', STR_PAD_RIGHT).'/100';
    endif;

    if ($decimal_number!='00/100'):
        $amount_in_words = strtoupper($whole_number).' PESOS & ' .$decimal_number.' ONLY';
    else:
        $amount_in_words = strtoupper($whole_number). ' PESOS ONLY';
    endif;
    return $amount_in_words;
}
function companyLogo()
{
    
    if (!auth()->guest()){
        $data = UserModel::getCompanyLogo();
        if($data){
            return $data->company_image;
        }
    }else{
        return false;
    }
}
function profilePicture()
{
    
    if (!auth()->guest()){
        $data = UserModel::getProfilePicture();
        if($data){
            return $data->image;
        }
    }else{
        return false;
    }
}

// function notificationAlerts()
// {
//     $functions      =   array('purchase_orders', 'purchases', 'payments', 'sale_orders', 'sales', 'receipts', 'general_journals', 'payment_tax');
//     $submitters     =   NotificationModel::getAllSubmitters($functions);
//     $data['submitters'] = [];
//     foreach ($submitters as $sub => $submitter) {
//         $submit_result = [];
//         foreach ($submitter[0] as $s => $submit) {
//             if ($submitter[1] == 'purchase_orders') {
//                 $link           =   '/purchases/purchase-orders?id='.$submit->id.'#view-purchase-order-modal';
//                 $tag            =   'Purchase Order : ';
//             }elseif ($submitter[1] == 'purchases') {
//                 if ($submit->purchase_tag == '0') {
//                     $link       =   '/purchases/purchase-invoices?id='.$submit->id.'#view-purchase-invoice-modal';
//                     $tag        =   'Purchase Invoice : ';
//                 }else{
//                     $link       =   '/purchases/other-payables?id='.$submit->id.'#view-other-payable-modal';
//                     $tag        =   'Other Payables : ';
//                 }
//             }elseif ($submitter[1] == 'payments') {
//                 if ($submit->payment_tag == '0') {
//                     $link       =   '/disbursements/payments?id='.$submit->id.'#view-payment-modal';
//                     $tag        =   'Payments : ';
//                 }else{
//                     $link       =   '/disbursements/payments-others?id='.$submit->id.'#view-payment-others-modal';
//                     $tag        =   'Payment Others : ';
//                 }
//             }elseif ($submitter[1] == 'sale_orders') {
//                 $link           =   '/sales/sale-orders?id='.$submit->id.'#view-sale-order-modal';
//                 $tag            =   'Sales Order : ';
//             }elseif ($submitter[1] == 'sales') {
//                 if ($submit->sale_tag == '0') {
//                     $link       =   '/sales/sale-invoices?id='.$submit->id.'#view-sale-invoice-modal';
//                     $tag        =   'Sales Invoice : ';
//                 }else{
//                     $link       =   '/sales/other-receivables?id='.$submit->id.'#view-other-receivable-modal';
//                     $tag        =   'Other Receivables : ';
//                 }
//             }elseif ($submitter[1] == 'receipts') {
//                 if ($submit->receipt_tag == '0') {
//                     $link       =   '/receipts/collections?id='.$submit->id.'#view-collection-modal';
//                     $tag        =   'Collections : ';
//                 }else{
//                     $link       =   '/receipts/collections-others?id='.$submit->id.'#view-collection-others-modal';
//                     $tag        =   'Collection Others : ';
//                 }
//             }elseif ($submitter[1] == 'general_journals') {
//                 $link           =   '/general-journal?id='.$submit->id.'#view-general-journal-modal';
//                 $tag            =   'General Journals : ';
//             }elseif ($submitter[1] == 'payment_tax') {
//                 if ($submit->tax_tag == '0') {
//                     $link       =   '/disbursements/payments-taxes?id='.$submit->id.'#view-payment-vat-modal';
//                     $tag        =   'Payment Tax VAT : ';
//                 }else{
//                     $link       =   '/disbursements/payments-taxes?id='.$submit->id.'#view-payment-wht-modal';
//                     $tag        =   'Payment Tax WHT : ';
//                 }
//             }
//             $items              =   new \stdClass();
//             $user               =   UserModel::getUserData($submit->created_by);
//             $items->created_by  =   $user->name;
//             $items->image       =   $user->image;
//             $items->company_id  =   $user->company_id;
//             $items->link        =   $link;
//             $items->tag         =   $tag;
//             $items->id          =   $submit->number;
//             $items->created_at  =   $submit->created_at;
//             array_push($submit_result, $items);
//         }
//         array_push($data['submitters'], $submit_result);
//     }
//     $checkers       =   NotificationModel::getAllCheckers();
//     // dd($checkers);
//     if (isset($checkers)) {
//         $data['checkers'] = [];
//         foreach ($checkers as $che => $checker) {
//             $check_result = [];
//             foreach ($checker[0] as $c => $check) {
//                 if ($checker[1] == 'purchase_orders') {
//                     $link           =   '/purchases/purchase-orders?id='.$check->id.'#view-purchase-order-modal';
//                     $tag            =   'Purchase Order : ';
//                 }elseif ($checker[1] == 'purchases') {
//                     if ($check->purchase_tag == '0') {
//                         $link       =   '/purchases/purchase-invoices?id='.$check->id.'#view-purchase-invoice-modal';
//                         $tag        =   'Purchase Invoice : ';
//                     }else{
//                         $link       =   '/purchases/other-payables?id='.$check->id.'#view-other-payable-modal';
//                         $tag        =   'Other Payables : ';
//                     }
//                 }elseif ($checker[1] == 'payments') {
//                     if ($check->payment_tag == '0') {
//                         $link       =   '/disbursements/payments?id='.$check->id.'#view-payment-modal';
//                         $tag        =   'Payments : ';
//                     }else{
//                         $link       =   '/disbursements/payments-others?id='.$check->id.'#view-payment-others-modal';
//                         $tag        =   'Payment Others : ';
//                     }
//                 }elseif ($checker[1] == 'sale_orders') {
//                     $link           =   '/sales/sale-orders?id='.$check->id.'#view-sale-order-modal';
//                     $tag            =   'Sales Order : ';
//                 }elseif ($checker[1] == 'sales') {
//                     if ($check->sale_tag == '0') {
//                         $link       =   '/sales/sale-invoices?id='.$check->id.'#view-sale-invoice-modal';
//                         $tag        =   'Sales Invoice : ';
//                     }else{
//                         $link       =   '/sales/other-receivables?id='.$check->id.'#view-other-receivable-modal';
//                         $tag        =   'Other Receivables : ';
//                     }
//                 }elseif ($checker[1] == 'receipts') {
//                     if ($check->receipt_tag == '0') {
//                         $link       =   '/receipts/collections?id='.$check->id.'#view-collection-modal';
//                         $tag        =   'Collections : ';
//                     }else{
//                         $link       =   '/receipts/collections-others?id='.$check->id.'#view-collection-others-modal';
//                         $tag        =   'Collection Others : ';
//                     }
//                 }elseif ($checker[1] == 'general_journals') {
//                     $link           =   '/general-journal?id='.$check->id.'#view-general-journal-modal';
//                     $tag            =   'General Journals : ';
//                 }elseif ($checker[1] == 'payment_tax') {
//                     if ($check->tax_tag == '0') {
//                         $link       =   '/disbursements/payments-taxes?id='.$check->id.'#view-payment-vat-modal';
//                         $tag        =   'Payment Tax VAT : ';
//                     }else{
//                         $link       =   '/disbursements/payments-taxes?id='.$check->id.'#view-payment-wht-modal';
//                         $tag        =   'Payment Tax WHT : ';
//                     }
//                 }
//                 $items              =   new \stdClass();
//                 $user               =   UserModel::getUserData($check->created_by);
//                 $items->created_by  =   $user->name;
//                 $items->image       =   $user->image;
//                 $items->company_id  =   $user->company_id;
//                 $items->link        =   $link;
//                 $items->tag         =   $tag;
//                 $items->id          =   $check->number;
//                 $items->created_at  =   $check->created_at;
//                 array_push($check_result, $items);
//             }
//             array_push($data['checkers'], $check_result);
//         }
//     }else{
//         $data['checkers'] = [];
//     }
    
//     $approvers       =   NotificationModel::getAllApprovers();
//     // dd($approvers);
//     if (isset($approvers)) {
//         $data['approvers'] = [];
//         foreach ($approvers as $app => $approver) {
//             $approve_result = [];
//             foreach ($approver[0] as $a => $approve) {
//                 if ($approver[1] == 'purchase_orders') {
//                     $link           =   '/purchases/purchase-orders?id='.$approve->id.'#view-purchase-order-modal';
//                     $tag            =   'Purchase Order : ';
//                 }elseif ($approver[1] == 'purchases') {
//                     if ($approve->purchase_tag == '0') {
//                         $link       =   '/purchases/purchase-invoices?id='.$approve->id.'#view-purchase-invoice-modal';
//                         $tag        =   'Purchase Invoice : ';
//                     }else{
//                         $link       =   '/purchases/other-payables?id='.$approve->id.'#view-other-payable-modal';
//                         $tag        =   'Other Payables : ';
//                     }
//                 }elseif ($approver[1] == 'payments') {
//                     if ($approve->payment_tag == '0') {
//                         $link       =   '/disbursements/payments?id='.$approve->id.'#view-payment-modal';
//                         $tag        =   'Payments : ';
//                     }else{
//                         $link       =   '/disbursements/payments-others?id='.$approve->id.'#view-payment-others-modal';
//                         $tag        =   'Payment Others : ';
//                     }
//                 }elseif ($approver[1] == 'sale_orders') {
//                     $link           =   '/sales/sale-orders?id='.$approve->id.'#view-sale-order-modal';
//                     $tag            =   'Sales Order : ';
//                 }elseif ($approver[1] == 'sales') {
//                     if ($approve->sale_tag == '0') {
//                         $link       =   '/sales/sale-invoices?id='.$approve->id.'#view-sale-invoice-modal';
//                         $tag        =   'Sales Invoice : ';
//                     }else{
//                         $link       =   '/sales/other-receivables?id='.$approve->id.'#view-other-receivable-modal';
//                         $tag        =   'Other Receivables : ';
//                     }
//                 }elseif ($approver[1] == 'receipts') {
//                     if ($approve->receipt_tag == '0') {
//                         $link       =   '/receipts/collections?id='.$approve->id.'#view-collection-modal';
//                         $tag        =   'Collections : ';
//                     }else{
//                         $link       =   '/receipts/collections-others?id='.$approve->id.'#view-collection-others-modal';
//                         $tag        =   'Collection Others : ';
//                     }
//                 }elseif ($approver[1] == 'general_journals') {
//                     $link           =   '/general-journal?id='.$approve->id.'#view-general-journal-modal';
//                     $tag            =   'General Journals : ';
//                 }elseif ($approver[1] == 'payment_tax') {
//                     if ($approve->tax_tag == '0') {
//                         $link       =   '/disbursements/payments-taxes?id='.$approve->id.'#view-payment-vat-modal';
//                         $tag        =   'Payment Tax VAT : ';
//                     }else{
//                         $link       =   '/disbursements/payments-taxes?id='.$approve->id.'#view-payment-wht-modal';
//                         $tag        =   'Payment Tax WHT : ';
//                     }
//                 }
//                 $items              =   new \stdClass();
//                 $user               =   UserModel::getUserData($approve->created_by);
//                 $items->created_by  =   $user->name;
//                 $items->image       =   $user->image;
//                 $items->company_id  =   $user->company_id;
//                 $items->link        =   $link;
//                 $items->tag         =   $tag;
//                 $items->id          =   $approve->number;
//                 $items->created_at  =   $approve->created_at;
//                 array_push($approve_result, $items);
//             }
//             array_push($data['approvers'], $approve_result);
//         }
//     }else{
//         $data['approvers'] = [];   
//     }
    
//     return $data;
// }
