<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class GlobalModel extends Model
{
    //get all rows
    public static function getDataTable($table='', $order='')
    {
        return DB::table($table)
                ->where('company_id', Auth::user()->company_id)
                ->orderBy('id', $order)
                ->get()->toArray();
    }

    //get all active rows
    public static function getActiveDataTable($table='', $order='')
    {
        return DB::table($table)
                ->where('status', '1')
                ->where('company_id', Auth::user()->company_id)
                ->orWhere('company_id', 0)
                ->orderBy('id', $order)
                ->get()->toArray();
    }

    //get single row
    public static function getSingleDataTable($table='', $id='')
    {
        return DB::table($table)
                ->where('id', $id)
                ->first();
    }

    //get multiple row
    public static function getDataTableWithID($table='', $id='')
    {
        return DB::table($table)
                ->where('id', $id)
                ->get()->toArray();
    }

    //global create
    public static function addDataTable($table='', $data='')
    {
                DB::table($table)
                ->insert($data);
        return DB::getPdo()->lastInsertId();
    }

    //global update
    public static function updateDataTable($table='', $data='')
    {
                DB::table($table)
                ->where('id', $data['id'])
                ->update($data);
        return  DB::table($table)
                ->where('id', $data['id'])
                ->first();
    }

    //global delete
    public static function deleteDataTable($table='', $id='')
    {
        return DB::table($table)
                ->where('id', $id)
                ->delete();
    }

    public static function checkIfCodeExists($table='', $code='')
    {
        return DB::table($table)
                ->where('code', $code)
                ->where('company_id', Auth::user()->company_id)
                ->count();
    }
}
