function sweetAlertAjax(message, post_data, method, url, success_callback, error_callback)
{
    swal({
        title: "Are you sure?",
        text: message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willSave) => {
        if (willSave) {
            globalAjax(post_data, method, url, success_callback, error_callback);
        } else {
            swal("Aborted!");
        }
    });
}

function sweetAlertAjaxUpload(message, post_data, method, url, success_callback, error_callback)
{
    swal({
        title: "Are you sure?",
        text: message,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willSave) => {
        if (willSave) {
            globalAjaxUpload(post_data, method, url, success_callback, error_callback);
        } else {
            swal("Aborted!");
        }
    });
}

function globalAjax(post_data, method, url, success_callback, error_callback)
{
    $.ajax({
        type: method,
        url: url,
        data: post_data,
        cache: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend : function() {
            $('.loader').show();
            var span = document.createElement("span");
            span.innerHTML = '<span class="loading-animation">LOADING...</span>';
            swal({
                content: span,
                icon: "warning",
                buttons: false,
                closeOnClickOutside: false
            });
            $('form :input').attr('disabled', 'disabled');
        },
        success: function (data) {
            success_callback(data);
        },
        error: function (data) {
            error_callback(data);
        },
        complete : function() {
            $('.loader').hide();
            $('form :input').removeAttr('disabled', 'disabled');
        }
    });
}

function globalAjaxWithoutValidation(post_data, method, url, success_callback, error_callback)
{
    $.ajax({
        type: method,
        url: url,
        data: post_data,
        cache: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend : function() {
            $('.loader').show();
        },
        success: function (data) {
            success_callback(data);
        },
        error: function (data) {
            error_callback(data);
        },
        complete : function() {
            $('.loader').hide();
        }
    });
}

function globalAjaxUpload(post_data, method, url, success_callback, error_callback)
{
    $.ajax({
        type: method,
        url: url,
        data: post_data,
        cache: false,
        contentType: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend : function() {
            $('.loader').show();
            var span = document.createElement("span");
            span.innerHTML = '<span class="loading-animation">LOADING...</span>';
            swal({
                content: span,
                icon: "warning",
                buttons: false,
                closeOnClickOutside: false
            });
        },
        success: function (data) {
            success_callback(data);
        },
        error: function (data) {
           success_callback(data);
        },
        complete : function() {
            $('.loader').hide();
        }
    });
}

function sweet_success()
{
    swal("Success!", {
        icon: "success",
    });
}

function sweet_error()
{
    swal("Error!", {
        icon: "error",
    });
}